from typing import Optional
from fastapi import APIRouter, HTTPException, Depends, status
from fastapi.security import APIKeyHeader
from app.api import api
from dotenv import load_dotenv, find_dotenv
import os

try:
    env_file = find_dotenv("app/key.env")
    load_dotenv(env_file)
    API_KEY = os.getenv("API_KEY")
    assert API_KEY is not None, "API_KEY must be set in the environment"
except Exception as e:
    print(f"Error loading environment variables: {str(e)}")
    raise

assert API_KEY is not None, "API_KEY must be set in the environment"

router = APIRouter()
api_key_header = APIKeyHeader(name="X-API-KEY")


@router.get("/secret")
def protected(api_key: Optional[str] = Depends(api_key_header)):

    if api_key != API_KEY:
        raise HTTPException(status_code=status.HTTP_401_UNAUTHORIZED,
                            detail="Invalid API key")
    secret = api.print_secret()
    return {"msg": secret.decode("utf-8")}
